#### Variables ####
$pathToZip=".\telegraf-1.27.0_windows_amd64.zip"
$targetDir="C:\Program Files\InfluxData\telegraf\"
$WINHOST=(Get-CIMInstance CIM_ComputerSystem).Name
$PROMP_ENV= Read-Host -Prompt "Environment name"
$TELEGRAF_ENVIRONMENT='  env            = "'+$PROMP_ENV+'" '
$TELEGRAF_HOSTNAME='  hostname            = "'+$WINHOST+'" '
###################

#### Download and expand Telegraf ####
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls, [Net.SecurityProtocolType]::Tls11, [Net.SecurityProtocolType]::Tls12, [Net.SecurityProtocolType]::Ssl3
[Net.ServicePointManager]::SecurityProtocol = "Tls, Tls11, Tls12, Ssl3"
Invoke-WebRequest "http://dl.influxdata.com/telegraf/releases/telegraf-1.27.0_windows_amd64.zip" -OutFile telegraf-1.27.0_windows_amd64.zip -UseBasicParsing 

# This expand option work only with powershell 5 (included in WS-2016 not work in WS-2012 unless powershell is updated)
# Expand-Archive .\telegraf-1.27.0_windows_amd64.zip -DestinationPath 'C:\Program Files\InfluxData\telegraf\'
# This option work with windows 2012 and 2016
[System.Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null
[System.IO.Compression.ZipFile]::ExtractToDirectory($pathToZip, $targetDir)

#### Create Telegraf.conf ####
New-Item "$targetDir\telegraf.conf" -ItemType "file"
Set-Content "$targetDir\telegraf.conf" '[global_tags]'
Add-Content "$targetDir\telegraf.conf" $TELEGRAF_ENVIRONMENT
Add-Content "$targetDir\telegraf.conf" '[agent]'
Add-Content "$targetDir\telegraf.conf" $TELEGRAF_HOSTNAME
Add-Content "$targetDir\telegraf.conf" '  interval            = "1m"'
Add-Content "$targetDir\telegraf.conf" '  round_interval      = true'
Add-Content "$targetDir\telegraf.conf" '  metric_batch_size   = 1000'
Add-Content "$targetDir\telegraf.conf" '  metric_buffer_limit = 10000'
Add-Content "$targetDir\telegraf.conf" '  collection_jitter   = "0s"'
Add-Content "$targetDir\telegraf.conf" '  flush_interval      = "30s"'
Add-Content "$targetDir\telegraf.conf" '  flush_jitter        = "0s"'
Add-Content "$targetDir\telegraf.conf" '  precision           = ""'
Add-Content "$targetDir\telegraf.conf" '  debug               = false'
Add-Content "$targetDir\telegraf.conf" '  quiet               = false'
Add-Content "$targetDir\telegraf.conf" '  logfile             = ""'
Add-Content "$targetDir\telegraf.conf" '[[outputs.http]]'
Add-Content "$targetDir\telegraf.conf" '  url         = "https://whisbi-tsdb-ingest.teramonitor.io/api/v1/push"'
Add-Content "$targetDir\telegraf.conf" '  method      = "POST"'
Add-Content "$targetDir\telegraf.conf" '  data_format = "prometheusremotewrite"'
Add-Content "$targetDir\telegraf.conf" '  username    = "whisbi"'
Add-Content "$targetDir\telegraf.conf" '  password    = "lL61SovK1sjT7WRl5XOV"'
Add-Content "$targetDir\telegraf.conf" '  [outputs.http.headers]'
Add-Content "$targetDir\telegraf.conf" '    Content-Type = "application/x-protobuf"'
Add-Content "$targetDir\telegraf.conf" '    Content-Encoding = "snappy"'
Add-Content "$targetDir\telegraf.conf" '    X-Prometheus-Remote-Write-Version = "0.1.0"'
Add-Content "$targetDir\telegraf.conf" '[[inputs.cpu]]'
Add-Content "$targetDir\telegraf.conf" '  percpu           = true'
Add-Content "$targetDir\telegraf.conf" '  totalcpu         = true'
Add-Content "$targetDir\telegraf.conf" '  collect_cpu_time = false'
Add-Content "$targetDir\telegraf.conf" '  report_active    = false'
Add-Content "$targetDir\telegraf.conf" '[[inputs.diskio]]'
Add-Content "$targetDir\telegraf.conf" '[[inputs.kernel]]'
Add-Content "$targetDir\telegraf.conf" '[[inputs.mem]]'
Add-Content "$targetDir\telegraf.conf" '[[inputs.processes]]'
Add-Content "$targetDir\telegraf.conf" '[[inputs.swap]]'
Add-Content "$targetDir\telegraf.conf" '[[inputs.system]]'
Add-Content "$targetDir\telegraf.conf" '[[inputs.disk]]'
Add-Content "$targetDir\telegraf.conf" '  ignore_fs = ["tmpfs", "devtmpfs", "devfs", "iso9660", "overlay", "aufs", "squashfs"]'

#### Move file and Install Telegraf ####
cd 'C:\Program Files\InfluxData\telegraf'
mv .\telegraf-1.27.0\telegraf.exe .
.\telegraf.exe --service install --config "C:\Program Files\InfluxData\telegraf\telegraf.conf"

#### Start Telegraf Service ####
net start telegraf