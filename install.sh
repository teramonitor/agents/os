#!/bin/bash
set -e

# ----- Config -----

export TELEGRAF_VERSION="1.27.4"
export PROMTAIL_VERSION="2.5.0"
export TSDB_ENDPOINT_SUFFIX="-tsdb-ingest.teramonitor.io/api/v1/push"
export LOGS_ENDPOINT_SUFFIX="-logs-ingest.teramonitor.io"
export TMP_DIR="/tmp"


# ----- Functions -----

function usage() {
    set -e
    cat <<EOM
    ##### install.sh #####
    Simple script to install TeraMonitor agents in a Linux machine

    Required parameters for installation
        -t  | --tenant-name      
        -mp | --metrics-password
        -lp | --logs-password
        -e  | --environment
    
    Uninstall
        -u  | --uninstall
    
EOM
    exit 2
}

function require {
    command -v "$1" > /dev/null 2>&1 || {
        echo "Some of the required software is not installed:"
        echo "    please install $1" >&2;
        exit 1;
    }
}

function require_variables {
   while [[ $# -gt 0 ]]; do
       var=$1
       if [ -z "${!var}" ]; then
          echo "$1 missing"
          exit 1
        fi
   shift
   done
}

function create_user {
    echo "Creating TeraMonitor user"
    sudo useradd --system teramonitor || echo "TeraMonitor user already present"
    sudo usermod -a -G docker teramonitor || echo "TeraMonitor user already in docker group"
    sudo usermod -a -G adm teramonitor || echo "TeraMonitor user already in adm group"
}

function install_telegraf {
    echo "Installing Metrics agent"
    cd ${TMP_DIR}
    wget "https://dl.influxdata.com/telegraf/releases/telegraf-${TELEGRAF_VERSION}_linux_amd64.tar.gz"
    tar xf "telegraf-${TELEGRAF_VERSION}_linux_amd64.tar.gz" ./telegraf-${TELEGRAF_VERSION}/usr/bin/telegraf
    sudo mv ./telegraf-${TELEGRAF_VERSION}/usr/bin/telegraf /usr/local/bin/
    sudo chown teramonitor:teramonitor /usr/local/bin/telegraf
    rm -r -f ./telegraf-${TELEGRAF_VERSION}*
    cd -
}

function install_promtail {
    echo "Installing Logs agent"
    cd ${TMP_DIR}
    wget "https://github.com/grafana/loki/releases/download/v${PROMTAIL_VERSION}/promtail-linux-amd64.zip"
    unzip promtail-linux-amd64.zip
    sudo mv promtail-linux-amd64 /usr/local/bin/promtail
    sudo chown teramonitor:teramonitor /usr/local/bin/promtail
    rm -r -f promtail-linux-amd64.zip
    cd -
}

function configure_environment {
    echo "Configuring TeraMonitor environment"
    sudo touch /etc/default/teramonitor
    echo "HOSTNAME"="${HOSTNAME}" | sudo tee /etc/default/teramonitor > /dev/null
    echo "TM_ENVIRONMENT=${TM_ENVIRONMENT}" | sudo tee -a /etc/default/teramonitor > /dev/null
    echo "TSDB_ENDPOINT=https://${TENANT}${TSDB_ENDPOINT_SUFFIX}" | sudo tee -a /etc/default/teramonitor > /dev/null
    echo "TSDB_USER=${TENANT}" | sudo tee -a /etc/default/teramonitor > /dev/null
    echo "TSDB_PASSWORD=${TSDB_PASSWORD}" | sudo tee -a /etc/default/teramonitor > /dev/null
    echo "LOGS_ENDPOINT=${TENANT}${LOGS_ENDPOINT_SUFFIX}" | sudo tee -a /etc/default/teramonitor > /dev/null
    echo "LOGS_USER=${TENANT}" | sudo tee -a /etc/default/teramonitor > /dev/null
    echo "LOGS_PASSWORD=${LOGS_PASSWORD}" | sudo tee -a /etc/default/teramonitor > /dev/null
    sudo chown teramonitor:teramonitor /etc/default/teramonitor
}

function configure_agents {
    echo "Configuring TeraMonitor agents"
    sudo rm -r -f /tmp/positions.yaml
    sudo mkdir -p /etc/teramonitor/telegraf.d
    sudo cp config/logs/log-shipper.yml /etc/teramonitor/
    sudo cp config/metrics/telegraf.conf /etc/teramonitor/
    sudo cp config/metrics/conf.d/*.conf /etc/teramonitor/telegraf.d/
}

function install_services {
    echo "Installing TeraMonitor services"
    sudo rm -rf /etc/systemd/system/telegraf.service || echo "Metric agent service not found"
    sudo cp linux/telegraf.service /etc/systemd/system/ || echo "Metric agent files found - Skipping"
    sudo rm -rf /etc/systemd/system/promtail.service || echo "Logs agent service not found"
    sudo cp linux/promtail.service /etc/systemd/system/ || echo "Logs agent service files found - Skipping"
    sudo systemctl daemon-reload
    sudo systemctl enable telegraf || echo "Metrics agent already active - Skipping"
    sudo systemctl restart telegraf || echo "Metrics agent already started - Skipping"
    sudo systemctl enable promtail || echo "Logs agent already active - Skipping"
    sudo systemctl restart promtail || echo "Logs agent already started - Skipping"
}

function uninstall {
    echo "Stopping TeraMonitor Services"
    sudo systemctl stop telegraf > /dev/null 2>&1 || echo "Unable to stop Metrics agent - Skipping"
    sudo systemctl disable telegraf > /dev/null 2>&1 || echo "Unable to disable Metrics agent - Skipping"
    sudo systemctl stop promtail > /dev/null 2>&1 || echo "Unable to stop Logs agent - Skipping"
    sudo systemctl disable promtail > /dev/null 2>&1 || echo "Unable to disable Logs agent - Skipping"
    echo "Uninstalling TeraMonitor Services"
    sudo rm -rf /etc/systemd/system/telegraf.service > /dev/null 2>&1 || echo "Unable to remove Metric agent - Skipping"
    sudo rm -rf /etc/systemd/system/promtail.service > /dev/null  2>&1 || echo "Unable to remove Logs agent - Skipping"
    sudo systemctl daemon-reload > /dev/null 2>&1
    echo "Removing TeraMonitor configuration"
    sudo rm -rf /etc/teramonitor > /dev/null 2>&1 || echo "Unable to remove TeraMonitor configuration - Skipping"
    sudo rm -rf /etc/default/teramonitor > /dev/null 2>&1 || echo "Unable to remove TeraMonitor configuration - Skipping"
    echo "Removing binaries"
    sudo rm -rf /usr/local/bin/telegraf > /dev/null 2>&1 || echo "Unable to remove Metrics agent binary - Skipping"
    sudo rm -rf /usr/local/bin/promtail > /dev/null 2>&1 || echo "Unable to remove Logs agent binary - Skipping"
    echo "Deleting TeraMonitor user"
    sudo userdel teramonitor > /dev/null 2>&1 || echo "Unable to remove TeraMonitor user - Skipping"
    echo "TeraMonitor uninstalled"

}


# ----- Main routine -----
# Check requirements
require wget

# Check parameters
if [ $# == 0 ]; then usage; fi

# Loop through arguments, two at a time for key and value
while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        -u|--uninstall)
            export TM_UNINSTALL="true"
            break
            ;;
        -t|--tenant-name)
            export TENANT="$2"
            shift 
            ;;
        -mp|--metrics-password)
            export TSDB_PASSWORD="$2"
            shift 
            ;;
        -lp|--logs-password)
            export LOGS_PASSWORD="$2"
            shift 
            ;;
        -e|--environment)
            export TM_ENVIRONMENT="$2"
            shift 
            ;;
        *)
            echo "ERROR: $1 is not a valid option"
            usage
        ;;
    esac
    shift 
done


#Check if we have all variables
if [[ ${TM_UNINSTALL} == "true" ]]; then
  uninstall
else
  echo "=====>  Starting TeraMonitor agent installation <====="
  echo "please enter the sudo password when prompted"
  require_variables TENANT TSDB_PASSWORD LOGS_PASSWORD TM_ENVIRONMENT
  create_user
  install_telegraf
  install_promtail
  configure_environment
  configure_agents
  install_services
fi
echo "All finished!"