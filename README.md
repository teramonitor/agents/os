# TeraMonitor OS Agent

## Requirements

Linux:

- [ ] SystemD based system
- [ ] AMD64 architechture (more in the workings)
- [ ] TeraMonitor Credentials


# Usage Installation

./install
        -t  | --tenant-name
        -mp | --metrics-password  
        -lp | --logs-password
        -e  | --environment
# Uninstallation

./install
        -u  | --uninstall
